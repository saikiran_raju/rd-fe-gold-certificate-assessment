import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiveBooksComponent } from './active-books.component';

describe('ActiveBooksComponent', () => {
  let component: ActiveBooksComponent;
  let fixture: ComponentFixture<ActiveBooksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActiveBooksComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ActiveBooksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
