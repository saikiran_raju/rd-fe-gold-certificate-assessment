import { Component } from '@angular/core';
import { Book } from 'src/book';
import { BookService } from 'src/book.service';

@Component({
  selector: 'app-active-books',
  templateUrl: './active-books.component.html',
  styleUrls: ['./active-books.component.css']
})
export class ActiveBooksComponent {
  books:Book[]=[];
  btnName="Delete";
  constructor(private bookService:BookService){

  }

  ngOnInit() : void{
    this.getBooks();
  }

  getBooks():void{
    this.bookService.getBooks().subscribe(data=>{
      this.books=data;
    })
  }
  
  deActivate = (book:Book):void=>{
    this.bookService.deleteBook(book.id).subscribe(book=>{
      this.getBooks();
    });
  }
  searchHere(){

  }
}
