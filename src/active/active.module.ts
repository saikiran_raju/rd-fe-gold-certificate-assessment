import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActiveBooksComponent } from './active-books/active-books.component';
import { SharedModule } from 'src/shared/shared.module';



@NgModule({
  declarations: [
    ActiveBooksComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class ActiveModule { }
