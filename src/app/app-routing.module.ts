import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ActiveBooksComponent } from 'src/active/active-books/active-books.component';
import { DeletedBooksComponent } from 'src/delete/deleted-books/deleted-books.component';

const routes: Routes = [
  {path:'home', component:HomeComponent},
  {path:'active',component:ActiveBooksComponent},
  {path:'deleted',component:DeletedBooksComponent},
  {
    path:'manage',
    loadChildren:()=>import('../manage/manage.module').then(module=>module.ManageModule)
  },
  {path:'',redirectTo:'/home',pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
