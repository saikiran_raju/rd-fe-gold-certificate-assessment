import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookComponent } from './book.component';
import { Book } from 'src/book';
import { FullNamePipe } from '../pipes/full-name.pipe';

describe('BookComponent', () => {
  let component: BookComponent;
  let fixture: ComponentFixture<BookComponent>;
  let dummyBook:Book={
    id: '',
    title: '',
    firstName: '',
    lastName: '',
    price: 0,
    rating: 3,
    isDeleted: false,
    lastUpdated: new Date()
  }
  let element:HTMLElement;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookComponent, FullNamePipe ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BookComponent);
    component = fixture.componentInstance;
    component.book=dummyBook;
    element=fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("should have a dummy Book details as input",()=>{
    dummyBook={
      "id": "1",
      "title": "JavaScript Big Bank",
      "firstName": "Axel",
      "lastName": "Rauschmayer",
      "price": 200,
      "rating": 4,
      "isDeleted": false,
      "lastUpdated": new Date()
    },
    component.book=dummyBook;
    fixture.detectChanges();
    expect(component.book).toEqual(dummyBook);
  });

  it("should have full name",()=>{
    dummyBook={
      "id": "1",
      "title": "JavaScript Big Bank",
      "firstName": 'Axel',
      "lastName": 'Rauschmayer',
      "price": 200,
      "rating": 4,
      "isDeleted": false,
      "lastUpdated": new Date()
    },
    component.book=dummyBook;
    fixture.detectChanges();
    expect(component.book).toEqual(dummyBook);
    const fullName=element.querySelector('h5');
    expect(fullName?.textContent).toEqual(dummyBook.firstName+" "+dummyBook.lastName);
  });

  it("should have red border if user is deleted or else green border",()=>{
    expect(element.querySelector('.border-success')).toBeTruthy();
    dummyBook.isDeleted=true;
    fixture.detectChanges();
    expect(element.querySelector('.border-danger')).toBeTruthy();
  });

  it("should have the button name Activate and button should be success",()=>{
      component.btnName="Activate";
      fixture.detectChanges();
      expect(component.btnName).toEqual("Activate");
      expect(element.querySelector('.btn-success')).toBeTruthy();
      expect(element.querySelector('.btn-danger')).toBeFalsy();
  });

  it("should have the button name Deactivate and button should be danger",()=>{
    component.btnName="Deactivate";
    fixture.detectChanges();
    expect(component.btnName).toEqual("Deactivate");
    expect(element.querySelector('.btn-danger')).toBeTruthy();
    expect(element.querySelector('.btn-success')).toBeFalsy();
  });
});
