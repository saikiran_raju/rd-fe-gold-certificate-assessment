import { Pipe, PipeTransform } from '@angular/core';
import { Book } from 'src/book'

@Pipe({
  name: 'filterBook'
})
export class FilterPipe implements PipeTransform {

  transform(value: Book[],filterText:string) {
    return value.filter((book)=>{
      return filterText==='Delete' ? !book.isDeleted:book.isDeleted;
    });
  }

}
