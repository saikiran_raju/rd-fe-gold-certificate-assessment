import { FullNamePipe } from './full-name.pipe';

describe('FullNamePipe', () => {
  it('create an instance for FullName Pipe', () => {
    const pipe = new FullNamePipe();
    expect(pipe).toBeTruthy();
  });

  it("should display fullname of Author",()=>{
    const pipe=new FullNamePipe();
    expect(pipe.transform("Harry","Potter")).toEqual("Harry Potter");
  });
});
