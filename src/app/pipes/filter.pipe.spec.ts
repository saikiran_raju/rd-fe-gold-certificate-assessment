import { Book } from 'src/book';
import { FilterPipe } from './filter.pipe';

describe('FilterPipe', () => {
  it('create an instance for Filter Pipe', () => {
    const pipe = new FilterPipe();
    expect(pipe).toBeTruthy();
  });

  it("should filter the active books based on filtertext isDeleted",()=>{
    const pipe=new FilterPipe();
    let books:Book[]=[{
        "id": "1",
        "title": "JavaScript: The Good Parts",
        "firstName": "Douglas",
        "lastName": "Crockford",
        "price": 1000,
        "rating": 3,
        "isDeleted": false,
        "lastUpdated": new Date()
      },
      {
        "id": "2",
        "title": "Eloquent JavaScript, 3rd Edition",
        "firstName": "Marijn",
        "lastName": "Haverbeke",
        "price": 1099,
        "rating": 5,
        "isDeleted": false,
        "lastUpdated": new Date()
      }]
    let result=pipe.transform(books,"Delete");
    expect(result.length).toEqual(2);
  });

  it("should filter the deleted users based on filtertext isDeleted",()=>{
    const pipe=new FilterPipe();
    let books:Book[]=[{
        "id": "1",
        "title": "JavaScript: The Good Parts",
        "firstName": "Bradford",
        "lastName": "Ledner",
        "price": 999,
        "rating": 4,
        "isDeleted": true,
        "lastUpdated": new Date()
      },
      {
        "id": "2",
        "title": "JavaScript: The Good Parts",
        "firstName": "Bradford",
        "lastName": "Ledner",
        "price": 679,
        "rating": 5,
        "isDeleted": true,
        "lastUpdated": new Date()
      }]
    let result=pipe.transform(books,"Delete");
    expect(result.length).toEqual(0);
  });

  it("should return empty array",()=>{
    const pipe=new FilterPipe();
    let result=pipe.transform([],"Activate");
    expect(result).toEqual([]);
  });
});
