import { Component } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Book } from 'src/book';
import { BookService } from 'src/book.service';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.css']
})
export class BookDetailComponent {
  selectedBook?:Book;
  id?:string;

  constructor(private route:ActivatedRoute,private bookService:BookService){
    
  }
  ngOnInit(){
    this.route.paramMap.subscribe((params:ParamMap)=>{
      this.id=params.get("id") || "0";
      this.setSelectedBook(this.id);
    });
  }

  setSelectedBook(id:string){
    this.bookService.getBook(id).subscribe(book=>{
      this.selectedBook=book;
    });
  }

}
