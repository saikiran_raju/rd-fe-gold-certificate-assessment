import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Book } from 'src/book';
import { BookService } from 'src/book.service';

@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html',
  styleUrls: ['./edit-book.component.css']
})
export class EditBookComponent {
  id?:string;
  selectedBook?:Book;

  constructor(private route:ActivatedRoute,private bookService:BookService){

  }

  ngOnInit(){
    this.route.paramMap.subscribe((params:ParamMap)=>{
      this.id=params.get("id") || "0";
      this.setSelectedBook(this.id);
    })
  }

  editForm=new FormGroup({
    title:new FormControl(''),
    price:new FormControl(0)
  })

  setSelectedBook(id:string){
    this.bookService.getBook(id).subscribe(book=>{
      this.selectedBook=book;
      this.editForm.setValue({
        title:this.selectedBook?.title || '',
        price:this.selectedBook?.price || 0
      })
    })
  }

  updateBook(){
    let id=this.selectedBook!.id;
    let values=this.editForm.value;
    this.bookService.editBook(id,values).subscribe(u=>{
      this.setSelectedBook(id);
    })
  }


}
