import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Book } from 'src/book';
import { BookService } from 'src/book.service';

@Component({
  selector: 'app-manage-book',
  templateUrl: './manage-book.component.html',
  styleUrls: ['./manage-book.component.css']
})
export class ManageBookComponent {
  books:Book[]=[];
  btnName="Details";

    constructor(public bookService:BookService,private router:Router,private route:ActivatedRoute){

    }

    ngOnInit(){
      this.getBooks();
    }

    getBooks():void{
      this.bookService.getBooks().subscribe(data=>{
        this.books=data;
      })
    }

    onSelect = (book:Book):void=>{
      this.router.navigate([book.id],{relativeTo:this.route})
    }

    onEdit=(book:Book):void=>{
      this.router.navigate(["edit",book.id],{relativeTo:this.route})
    }

    createBook(){
      this.router.navigate(["create"],{relativeTo:this.route})
    }

}
