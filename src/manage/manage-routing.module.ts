import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManageBookComponent } from './manage-book/manage-book.component';
import { BookDetailComponent } from 'src/app/book-detail/book-detail.component';
import { EditBookComponent } from './edit-book/edit-book.component';
import { CreateBookComponent } from './create-book/create-book.component';

const routes: Routes = [
  {
    path:'',
    component:ManageBookComponent,
    children:[
      {
        path:'create',
        component:CreateBookComponent
      },
      {
        path:':id',
        component:BookDetailComponent
      },
      {
        path:'edit/:id',
        component:EditBookComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageRoutingModule { }
