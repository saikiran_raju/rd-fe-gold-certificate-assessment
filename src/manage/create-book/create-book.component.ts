import { Component } from '@angular/core';
import { FormBuilder,Validators } from '@angular/forms';
import { BookService } from 'src/book.service';

@Component({
  selector: 'app-create-book',
  templateUrl: './create-book.component.html',
  styleUrls: ['./create-book.component.css']
})
export class CreateBookComponent {
  constructor(private fb:FormBuilder,private bookService:BookService){

  }
  registrationForm=this.fb.group({
    title: ['', Validators.required],
    firstName:['',Validators.required],
    lastName:['',Validators.required],
    price:["",Validators.required],
    isDeleted:[false]
  })

  getUniqueId() : string{    
    let uniqueId=Date.now()+((Math.random()*100000).toFixed());
    return uniqueId;
  }   


  submitForm(){
    let now=new Date();
    let book=this.registrationForm.value;
    const newBook={
      id: this.getUniqueId(),
      ...book,
      lastUpdated: now
    }
    this.bookService.createBook(newBook).subscribe(u=>{
      this.registrationForm.reset();
      location.reload();
    });
  }

}
