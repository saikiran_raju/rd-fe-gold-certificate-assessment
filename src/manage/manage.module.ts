import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageBookComponent } from './manage-book/manage-book.component';
import { ManageRoutingModule } from './manage-routing.module';
import { SharedModule } from 'src/shared/shared.module';
import { CreateBookComponent } from './create-book/create-book.component';
import { EditBookComponent } from './edit-book/edit-book.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    CreateBookComponent,
    EditBookComponent,
    ManageBookComponent
  ],
  imports: [
    CommonModule,
    ManageRoutingModule,
    SharedModule,
    ReactiveFormsModule
  ]
})
export class ManageModule { }
