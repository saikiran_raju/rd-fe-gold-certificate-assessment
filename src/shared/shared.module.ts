import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookComponent } from 'src/app/book/book.component';
import { BookDetailComponent } from 'src/app/book-detail/book-detail.component';
import { BookListComponent } from 'src/app/book-list/book-list.component';
import { FilterPipe } from 'src/app/pipes/filter.pipe';
import { FullNamePipe } from 'src/app/pipes/full-name.pipe';
import { StatusPipe } from 'src/app/pipes/status.pipe';

@NgModule({
  declarations: [
    BookComponent,
    BookDetailComponent,
    BookListComponent,
    FilterPipe,
    FullNamePipe,
    StatusPipe,
  ],
  imports: [
    CommonModule
  ],
  exports:[
    BookComponent,
    BookDetailComponent,
    BookListComponent,
    FilterPipe,
    FullNamePipe,
    StatusPipe
  ]
})
export class SharedModule { }
