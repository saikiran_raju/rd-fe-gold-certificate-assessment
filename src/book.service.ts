import { Injectable } from '@angular/core';
import { Book } from './book';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';


@Injectable({
  providedIn: 'root'
})
export class BookService {
  booksURL = "http://localhost:3000/books";

  constructor(private http:HttpClient) { }

  getBooks():Observable<Book[]> {
    return this.http.get<Book[]>(this.booksURL);
  }

  getBook(id:string):Observable<Book>{
    const url=this.booksURL+"/"+id;
    return this.http.get<Book>(url);
  }

  deleteBook(id:string):Observable<Book>{
    const url=this.booksURL+"/"+id;
    let delBook=this.http.patch<Book>(url,{
      isDeleted:true
    })
    return delBook;
  }

  activateBook(id:string):Observable<Book>{
    const url=this.booksURL+"/"+id;
    let activatedBook=this.http.patch<Book>(url,{
      isDeleted:false
    })
    return activatedBook;
  }

  createBook(book:any):Observable<Book>{
    const url=this.booksURL;
    let newBook=this.http.post<Book>(url,{
      ...book
    });
    return newBook;
  }

  editBook(id:string,values:any):Observable<Book>{
    const url=this.booksURL+"/"+id;
    let now=new Date();
    let updatedBook=this.http.patch<Book>(url,{
      ...values,
      lastUpdated:now
    });
    return updatedBook;
  }
}
