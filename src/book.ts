export interface Book {
    id: string;
    title: string;
    firstName: string;
    lastName: string;
    price: number;
    rating: number;
    isDeleted: boolean;
    lastUpdated:Date;
}
  