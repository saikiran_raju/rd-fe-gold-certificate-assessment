import { Component } from '@angular/core';
import { Book } from 'src/book';
import { BookService } from 'src/book.service';

@Component({
  selector: 'app-deleted-books',
  templateUrl: './deleted-books.component.html',
  styleUrls: ['./deleted-books.component.css']
})
export class DeletedBooksComponent {

  books:Book[]=[];
  btnName="Activate";
  constructor(private bookService:BookService){

  }

  ngOnInit() : void{
    this.getDeletedBooks();
 }

 getDeletedBooks() : void{
  this.bookService.getBooks().subscribe(data=>{
     this.books=data;
  })
}

activate = (book:Book):void =>{
  this.bookService.activateBook(book.id).subscribe(book=>{
     this.getDeletedBooks();
  });
}

}
