import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeletedBooksComponent } from './deleted-books/deleted-books.component';
import { SharedModule } from 'src/shared/shared.module';



@NgModule({
  declarations: [
    DeletedBooksComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class DeleteModule { }
